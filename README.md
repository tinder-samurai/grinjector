# **GrInjector**  [![GitHub license](https://img.shields.io/badge/license-MIT-yellow.svg)](https://raw.githubusercontent.com/henryco/GrInjector/master/LICENSE)  [![Maven Central](https://img.shields.io/maven-central/v/com.github.henryco/grinjector.svg)](http://repo1.maven.org/maven2/com/github/henryco/grinjector/)  
##### Lightweight reflective dependency injector for java and Android, compatibile with [JSR-330](https://docs.oracle.com/cd/E19798-01/821-1841/gjxvg/index.html) specification.
###### [Wiki page](https://bitbucket.org/tinder-samurai/grinjector/wiki/)
  
  ____
  
## Installation 
You need to add maven central repository into your project and then add dependency:  
  
  
**Gradle**  
```Groovy
    compile 'com.github.henryco:grinjector:1.2.1'
```  
**Maven**  
```XML
    <dependency>
        <groupId>com.github.henryco</groupId>
        <artifactId>grinjector</artifactId>
        <version>1.2.1</version>
    </dependency>
```

____

### TODO: 

* Refactoring
* Scopes
