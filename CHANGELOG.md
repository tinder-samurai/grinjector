### 10-06-2018
#### Version 1.2.0 Release
* Added **`JSR-330`** Compatibility

____
### 22-01-2018
#### Version 1.1.0 Release

____
### 21-01-2018
#### Version 1.0.3, 1.0.4, 1.0.5
* Added **`CustomClassFinder`** interface and bug fix

____
### 20-01-2018
#### Version 1.0.2
* Added **`@Named`** annotation

____
### 15-01-2018
#### Version 1.0.1
* Added shared modules

____
### 13-01-2018 
#### Version 1.0.0
Release